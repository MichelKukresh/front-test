//импорт CSS --   >> для WEBPACK
import Tabs from "./components/Tabs";
import "./index.css";
//находим картинки
const imgtTabDrag = document.querySelector(".tabs__tab-drag");
const imgTabSmooth = document.querySelector(".tabs__content");
const d = document.querySelector(".tabs__tab-smooth");

//находим сами табы
const tabSmooth = document.querySelector("#tabs__tab-smooth");
const tabsDrag = document.querySelector("#tabs__tab-drag");

//находим кнопки
const buttonDrag = document.querySelector("#tabDrag");
const buttonSmooth = document.querySelector("#tabSmooth");
const selectDrag = document.querySelector("#selecTtabDrag");
const selectSmooth = document.querySelector("#selectTabSmooth");

//определяем ширину экрана
const screenWidth = window.screen.width;
const screenHeight = window.screen.height;

//механизм перетаскивания
let offsetX;
let offsetY;
imgtTabDrag.addEventListener("dragstart", function (e) {  
  offsetX = e.offsetX;
  offsetY = e.offsetY;
});
imgtTabDrag.addEventListener("dragend", function (e) {  
  imgtTabDrag.style.top = e.pageY - offsetY - 220 + "px";
  imgtTabDrag.style.left =
    e.pageX - offsetX - ((screenWidth - 880) / 2 + 16) + "px";
});
//механизм перемещения
imgTabSmooth.addEventListener("mousemove", (e) => {
  
  d.style.top = 950 - (((100 * e.clientY) / 400) * 1183) / 100 + "px";
  d.style.left = 1906 - (((100 * e.clientX) / 600) * 1906) / 100 + "px";
});

//переключение табов
buttonDrag.addEventListener("click", () => {
  tabSmooth.classList.remove("tabs__pane_show");
  tabsDrag.classList.add("tabs__pane_show");
  buttonSmooth.classList.remove("tabs__btn_active");
  buttonDrag.classList.add("tabs__btn_active");
});
buttonSmooth.addEventListener("click", () => {
  tabsDrag.classList.remove("tabs__pane_show");
  tabSmooth.classList.add("tabs__pane_show");
  buttonDrag.classList.remove("tabs__btn_active");
  buttonSmooth.classList.add("tabs__btn_active");
});

//механизм прокрутки хедера по условию.
const hederElement = document.querySelector(".header");
window.onscroll = function () {
  let posLeft =
    window.pageXOffset !== undefined
      ? window.pageXOffset
      : (document.documentElement || document.body.parentNode || document.body)
          .scrollLeft;
  let posTop =
    window.pageYOffset !== undefined
      ? window.pageYOffset
      : (document.documentElement || document.body.parentNode || document.body)
          .scrollTop;  
  hederElement.style.height = 150 - (((posTop * 100) / 1046) * 80) / 100 + "px";
};

//механизм переключения табов при select
document.querySelector(".tabs__nav-select").addEventListener("change", (e) => {
  let data = document.querySelector(".tabs__nav-select").value;  
  if (data == 1) {
    tabSmooth.classList.remove("tabs__pane_show");
    tabsDrag.classList.add("tabs__pane_show");
  } else {
    tabsDrag.classList.remove("tabs__pane_show");
    tabSmooth.classList.add("tabs__pane_show");
  }
});

//построение изображения внутри табов
const tabDragAndDropMechanism = new Tabs(); //механизм перетаскивания
const tabSmoothMovementMechanism = new Tabs(); //механизм плавного перемещения
